﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    public float speed;
    public Text countText;
    public Text winText;

    Renderer rend;

    private Rigidbody rb;
    private int count;

    public float jumpPower; //How high the ball's jump is
    bool onGround = false; //Whether the ball is on a surface or not

    int totalPickups;

    void Start ()
    {
        //Set up variables used in other functions
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText ();
        winText.text = "";
        rend = GetComponent<Renderer>();

        GameObject[] allPickups = GameObject.FindGameObjectsWithTag("Pick Up");
        totalPickups = allPickups.Length;
    }

    private void SetCountText()
    {
    }

    void Update()
    {
        onGround = Physics.Raycast(transform.position, Vector3.down, transform.localScale.x/2f + .5f);
        //Press Space to make Player jump if on ground
        if (Input.GetKeyDown(KeyCode.Space) && onGround)
        {
            Jump();
        }
    }

    void Jump()
     {
        rb.AddForce(Vector3.up * jumpPower);
     }

    //Update is called once per frame
    void FixedUpdate ()
    {
        //Player movement
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);
        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            //Add 1 to Count Text whenever a Pick Up is collected
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();

            //Increase Player speed by 2.5 for each Pick Up collected
            speed += 2.5f;

            //Increase total scale of Player by 0.12 for each Pick Up collected
            transform.localScale += new Vector3(0.12f, 0.12f, 0.12f);

            //Change Player material to Pick Up material on collision
            rend.material = other.gameObject.GetComponent<Renderer>().material;
        }

        //Restart scene when Player touches Death Plane
        if (other.gameObject.CompareTag("Death"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString ();
            //Show Win Text when all Pick Ups have been collected
            if (count >= totalPickups)
            {
                winText.text = "You Win!";
            }
        }
    }
}